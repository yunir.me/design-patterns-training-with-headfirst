package observer.weather_station;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CurrentStateDisplayTest {

    @Test
    void testObserverSubscribed() {
        WeatherData wd = new WeatherData(1, 2, 3, 4);
        CurrentStateDisplay csd = new CurrentStateDisplay(wd);
        assertEquals(1, csd.wdSubject.countObservers());
    }

    @Test
    void testMetricsSet() {
        WeatherData wd = new WeatherData(1, 2, 3, 4);
        CurrentStateDisplay csd = new CurrentStateDisplay(wd);
        assertEquals(wd.getTemp(), csd.temp);
        assertEquals(wd.getHumidity(), csd.hum);
    }

    @Test
    void testMetricsChanged() {
        WeatherData wd = new WeatherData(1, 2, 3, 4);
        CurrentStateDisplay csd = new CurrentStateDisplay(wd);
        wd.newData(4, 3, 2, 1);
        assertEquals(wd.getTemp(), csd.temp);
        assertEquals(wd.getHumidity(), csd.hum);
        wd.newData(1, 2, 3, 4);
        assertEquals(wd.getTemp(), csd.temp);
        assertEquals(wd.getHumidity(), csd.hum);
    }

}