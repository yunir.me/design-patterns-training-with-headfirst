package strategy.duck_tales;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import strategy.duck_tales.fly.NoFly;
import strategy.duck_tales.fly.RocketPowerFly;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class DuckTest {
    ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    PrintStream defaultOut = System.out;

    @BeforeEach
    public void setUpOutputStream() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public  void restoreOutStream() {
        System.setOut(defaultOut);
    }

    @Test
    void testChangedFlyBehavior() {
        Duck kingDuck = new KingDuck();
        //kingDuck.swim();  // check current behavior
        //kingDuck.fly();   // check current behavior
        kingDuck.setFlyBehaviour(new NoFly());
        kingDuck.fly();
        String actual = outContent.toString();

        outContent.reset();
        (new NoFly()).fly();
        String expected = outContent.toString();

        assertEquals(expected, actual);
    }

    @Test
    void testMimicDuckDevice() {
        DuckCall dc = new DuckCall();
        //dc.mimicDuck();   // check current behavior
        dc.setFb(new RocketPowerFly());
        dc.mimicDuck();

        String actual = outContent.toString();

        outContent.reset();
        (new RocketPowerFly()).fly();
        String expected = outContent.toString();

        assertEquals(expected, actual);
    }

}