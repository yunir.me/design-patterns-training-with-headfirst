package template_method.drinks;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DrinkTest {
    @Test
    void testTea() {
        Drink tea = new Tea();
        tea.prepare();
    }

    @Test
    void testCoffee() {
        Drink coffee = new Coffee();
        coffee.prepare();
    }
}