package state.gumball_machine;

public class OneCoinState implements State {
    GumballMachine gumballMachine;

    public OneCoinState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void putCoin() {
        System.out.println("You cant insert more than 1 coin");
    }

    @Override
    public void getCoin() {
        System.out.println("As you wish lord!");
        gumballMachine.setState(gumballMachine.noCoinState);
    }

    @Override
    public void rollCrank() {
        System.out.println("NOT IMPLEMENTED YET");
    }

    @Override
    public void dispense() {
        System.out.println("It is useless");
    }
}
