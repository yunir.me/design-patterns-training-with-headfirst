package state.gumball_machine;

public class EmptyMachineState implements State {
    @Override
    public void putCoin() {
        System.out.println("It is useless");
    }

    @Override
    public void getCoin() {
        System.out.println("It is useless");
    }

    @Override
    public void rollCrank() {
        System.out.println("It is useless");
    }

    @Override
    public void dispense() {
        System.out.println("It is useless");
    }
}
