package state.gumball_machine;

public interface State {
    void putCoin();
    void getCoin();
    void rollCrank();
    void dispense();
}
