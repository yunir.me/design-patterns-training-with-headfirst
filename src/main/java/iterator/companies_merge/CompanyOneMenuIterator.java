package iterator.companies_merge;

import java.util.ArrayList;
import java.util.Iterator;

public class CompanyOneMenuIterator implements Iterator<MenuItem> {
    ArrayList<MenuItem> items;
    int position = 0;

    public CompanyOneMenuIterator(ArrayList<MenuItem> menuItems) {
        this.items = menuItems;
    }

    @Override
    public boolean hasNext() {
        return position < items.size();
    }

    @Override
    public MenuItem next() {
        MenuItem item = items.get(position);
        position++;
        return item;
    }
}
