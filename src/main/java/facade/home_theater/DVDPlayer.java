package facade.home_theater;

public interface DVDPlayer {
    void turnOn();
    void loadMovie(String name);
    void turnOff();
}
