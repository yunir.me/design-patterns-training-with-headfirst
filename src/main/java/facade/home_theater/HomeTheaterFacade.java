package facade.home_theater;

public class HomeTheaterFacade {
    DVDPlayer dvdPlayer;
    PopCornDevice popCornDevice;
    Lights lights;

    void prepareTheatreMode(String movieName) {
        lights.off();
        popCornDevice.startBaking();
        dvdPlayer.turnOn();
        dvdPlayer.loadMovie(movieName);
    }

    void disableTheatreMode() {
        lights.on();
        popCornDevice.stopBaking();
        dvdPlayer.turnOff();
    }
}
