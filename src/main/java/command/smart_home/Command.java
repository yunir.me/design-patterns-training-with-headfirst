package command.smart_home;

public interface Command {
    void execute();
}
