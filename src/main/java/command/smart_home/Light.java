package command.smart_home;

public interface Light {
    void on();
    void off();
}
