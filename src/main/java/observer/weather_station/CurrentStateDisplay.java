package observer.weather_station;

import java.util.Observable;
import java.util.Observer;

public class CurrentStateDisplay implements Observer {
    int temp;
    int hum;
    Observable wdSubject;

    public CurrentStateDisplay(Observable wdSubject) {
        this.wdSubject = wdSubject;
        update(wdSubject, null);
        wdSubject.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData wd = (WeatherData) o;
            temp = wd.getTemp();
            hum = wd.getHumidity();
            display();
        }
    }

    public void display() {
        System.out.println("My temp = " + temp + " and hum = " + hum);
    }
}
