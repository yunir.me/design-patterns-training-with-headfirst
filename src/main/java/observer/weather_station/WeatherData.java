package observer.weather_station;

import java.util.Observable;

public class WeatherData extends Observable {
    private int temp;
    private int humidity;
    private int pressure;
    private int heatRate;

    public WeatherData(int temp, int humidity, int pressure, int heatRate) {
        newData(temp, humidity, pressure, heatRate);
    }

    public void newData(int temp, int humidity, int pressure, int heatRate) {
        this.temp = temp;
        this.humidity = humidity;
        this.pressure = pressure;
        this.heatRate = heatRate;
        setChanged();
        notifyObservers();
    }

    public int getTemp() {
        return temp;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getPressure() {
        return pressure;
    }

    public int getHeatRate() {
        return heatRate;
    }
}
