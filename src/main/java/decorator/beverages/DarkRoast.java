package decorator.beverages;

public class DarkRoast extends Beverage {

    public DarkRoast() {
        description = "The dark roast with tasty smell";
    }

    @Override
    public double cost() {
        return 100;
    }

    @Override
    public String toString() {
        return description;
    }

    @Override
    void prepare() {
        System.out.println("Prepare " + this.toString());
    }
}
