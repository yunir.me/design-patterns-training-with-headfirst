package factory.pizzas.notSimple;

public class NYPizzeria extends Pizzeria {
    @Override
    Pizza createPizza(String type) {
        if (type.equals("cheese"))
            return new CheesePizza();
        else
            return new UnknownPizza();
    }
}
