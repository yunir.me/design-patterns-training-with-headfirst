package factory.pizzas.notSimple;

public interface Pizza {
    void prepare();
    void bake();
    void cut();
    void box();
}
