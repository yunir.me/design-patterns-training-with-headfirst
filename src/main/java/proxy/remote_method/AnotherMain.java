package proxy.remote_method;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class AnotherMain {
    HelloRemote helloRemote;

    {
        try {
            helloRemote = (HelloRemote) Naming.lookup("rmi://127.0.0.1/hello");
            helloRemote.sayHello();
        } catch (NotBoundException | MalformedURLException | RemoteException e) {
            e.printStackTrace();
        }
    }
}
