package strategy.duck_tales;

import strategy.duck_tales.fly.RocketPowerFly;

public class KingDuck extends Duck {
    public KingDuck() {
        setFlyBehaviour(new RocketPowerFly());
    }
}
