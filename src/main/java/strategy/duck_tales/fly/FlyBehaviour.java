package strategy.duck_tales.fly;

public interface FlyBehaviour {
    void fly();
}
