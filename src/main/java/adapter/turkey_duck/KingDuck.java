package adapter.turkey_duck;

public class KingDuck implements Duck {
    @Override
    public void quack() {
        System.out.println("quack quack");
    }

    @Override
    public void fly() {
        System.out.println("flying long distance");
    }
}
