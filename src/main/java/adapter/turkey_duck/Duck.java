package adapter.turkey_duck;

public interface Duck {
    void quack();
    void fly();
}
