package template_method.drinks;

public class Tea extends Drink {
    @Override
    void brew() {
        System.out.println("Prepare tea with cup");
    }

    @Override
    void putCondiments() {
        System.out.println("Put some lemon");
    }
}
