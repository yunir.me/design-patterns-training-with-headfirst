package template_method.drinks;

public class Coffee extends Drink {
    @Override
    void brew() {
        System.out.println("Use coffee-machine");
    }

    @Override
    void putCondiments() {
        System.out.println("Put some milk and sugar");
    }

    @Override
    boolean customerWantsCondiments() {
        return false;
    }
}
