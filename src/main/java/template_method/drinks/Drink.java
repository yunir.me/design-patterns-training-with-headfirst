package template_method.drinks;

public abstract class Drink {
    final void prepare() {
        boilWater();
        brew();
        pourInCup();
        if (customerWantsCondiments()) {
            putCondiments();
        }
    }

    void boilWater() {
        System.out.println("Boil the water");
    }

    abstract void brew();

    void pourInCup() {
        System.out.println("Pouring into cup");
    }

    // hook-method - optional to implement
    boolean customerWantsCondiments() {
        return true;
    }

    abstract void putCondiments();

}
