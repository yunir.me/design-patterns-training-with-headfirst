# OOP design patterns

## OO Basics

- Abstraction
- Encapsulation
- Polymorphism
- Inheritance

## Principles

- Encapsulate (to separate class) what varies
- Program to interface, not implementation
- Favor composition (HAS-A) over inheritance (IS-A) (composition = possibility to use its subclasses too)
- Strive for loosely coupled designs (interfaces interaction) between objects that interact
- Classes should be open for extension (new stuff), but closed for modification (of existing logic)
- Depend on abstractions. Do not depend on concrete classes
- Principle of The Least Knowledge: talk only to your immediate friends
  (use methods of: self, components, parameters, created)
- The Hollywood Principle: Don’t call us, we’ll call you
- A class should have only one reason to change

## Patterns

### Strategy

Defines a family of algorithms, encapsulates each one, and makes them interchangeable (inside another class). Strategy
lets the algorithm vary independently from clients (another class) that use it.

- Extract different implementations of the same stuff (algorithm) to separate classes
- Add possibility to reuse implemented algorithm inside other classes via composition (class field)
- Easy to change logic at runtime

### Observer

Defines a one-to-many dependency between objects so that when one object (Observable) changes state, all its
dependents (Observers) are notified and updated automatically

- Implements `Observer` interface for looking one (aka Listener, Subscriber)
    - implement `update` function
- Implements `Observable` interface for changing one
    - call `setChanged` and `notifyAll` after made changes
- Loosely coupled: have little knowledge of each other

### Decorator

Attach additional (new) responsibilities fto an object dynamically. Decorators provide a flexible alternative to
subclassing for extending functionality.

- Add Subject's Interface as parameter to Extension class
- Extension class implements the same interface as of Subject class
- Call Subjects functions on Overridden functions in Extension class
- Wrap subject class with Extension constructor, and again, and again if you wish
- Extend functionality

### Factory

**Factory Method** defines an interface for creating an object, but lets subclasses decide which class to instantiate.
Factory Method lets a class defer instantiation to subclasses.

**Abstract Factory** provides an interface for creating families of related or dependent objects without specifying
their concrete classes.

![img.png](images/abstract-factory.png)
![img_1.png](images/abstract-factory-impl.png)

### Singleton

Ensures a class has only one instance, and provides a global point (public) of access to it.

- Consist of: a private constructor, a static method combined with a static variable

### Command

Encapsulates a request as an object, thereby letting you parameterize other objects with different requests, queue or
log requests, and support undoable operations.

- May consists of encapsulated commands inside another command (macro)
- Possible to implement undo of command inside command

### Adapter

Converts the interface(s) of a class(es) into another interface the clients expect. Adapter lets classes work together
that could not otherwise because of incompatible interfaces.

- Change and unify interfaces

**Use-case**: old `Enumeration` to new `Iterator` class

**Object Adapter**:
![img.png](images/adapter-object.png)

**Class Adapter**  (multiple inheritance is not possible in Java):
![img.png](images/adapter-class.png)

### Facade

Provides a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes
the subsystem easier to use.

- Simplifies life

### Template Method

Defines the skeleton of an algorithm in a method, deferring some steps to subclasses. Template Method lets subclasses
redefine certain steps of an algorithm without changing the algorithm’s structure.

- Method should be `final`
- Use hook methods (that do nothing by default) to extend template method

**Use-cases**:

- `Arrays.sort()` template method with concrete `swap()` step and `compareTo()` step to implement by a client (
  use `Comparable`); But this primer is w/o typical inheritance
- Swing `JFrame` has `update` algorithm and `paint()` hook-method
- Old `Applets` with hook methods (also has `paint()` method)
- Factory method pattern

### Iterator

Provides a way to access the elements of an aggregate object sequentially without exposing its underlying
representation.

- Internal Iterator is not possible to control by itself, it's logic is inside something
- External Iterator allows to use next and hasNext and traverse as user wants

### Composition

Allows you to compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat
individual objects and compositions of objects uniformly.

### State

Allows an object to alter its behavior when its internal state changes. The object will appear to change its class.

### Proxy

Provides a surrogate or placeholder (stub and skeleton) for another object to control access to it.

- Create an interface with needed functions that implement `Remote` interface
- Checkout that everything is serializable
- Set `RemoteException` to all functions (of interface) and constructor (for implementation)
- Extend `UnicastRemoteObject` and set `SerialVersionUID`
- To execute binding and generating stub and skeleton - use `Naming.rebind("name", object)`
- Do not forget to run `rmiregistry` first
- To retrieve stub - use `(RemoteInterfaceCast) Naming.lookup("rmi://127.0.0.1/name")`

Types:
- Remote Proxy
- Virtual Proxy
  - Caching Proxy
- Protection Proxy 
  - Dynamic Proxy (`java.lang.reflect`)

Also:
- Firewall Proxy
- Smart Reference Proxy
- Synchronization Proxy
- Complexity hiding (Facade) Proxy
- Copy-on-Write Proxy
- e.t.c.

### Compound
**MVC (Model-View-Controller)** - consist of **Observer**, **Strategy** and **Composite** patterns
- Model is Observable
- View has composite tree of objects 
- Every action of the View goes through Controller via Strategy pattern
- View reacts to Model changes of state via subscribing (Observer)
- Controller is a mediator between Model and View - gives the API (via an interface) for the View to work with Model

## Other patterns

### Bridge
Vary not only your implementations, but also your abstractions.

### Builder
Encapsulate the construction of a product and allow it to be constructed in steps.

### Chain of responsibility
Give more than one object a chance to handle a request

### Flyweight
One instance of a class can be used to provide many virtual instances.

### Interpreter
Build an interpreter for a language.

### Mediator
Centralize complex communications and control between related objects.

### Memento
Return an object to one of its previous states; for instance, if your user requests an “undo.”

### Prototype
When creating an instance of a given class is either expensive or complicated.

### Visitor
Add capabilities to a composite of objects and encapsulation is not important.